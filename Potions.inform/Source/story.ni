"Potions" by "Daniel M. Stelzer"

[

Include Measured Liquid by Emily Short.
Include Typographical Conveniences by Daniel Stelzer.
Include Assorted Text Generation by Emily Short.

Use American dialect and the serial comma.
Use mixed liquids and variable sip size.

Volume Zero - Replacements

Section 1X - Definition (in place of Section 1I - Definition (for use without Metric Units by Graham Nelson) in Measured Liquid by Emily Short)

A volume is a kind of value. 1.0 dram (singular) specifies a volume. 1.0 drams (plural) specifies a volume. 1.0z specifies a volume. 1.0 z specifies a volume.

A fluid container has a volume called a fluid capacity. A fluid container has a volume called fluid content. The fluid capacity of a fluid container is usually 96z. The fluid content of a fluid container is usually 0z.

The sip size is a volume that varies. The sip size is initially 12 dram.

The max volume is always 2147483647 dram.

Section 2 - Stuff

The standard report pouring rule response (E) is "[if the noun is empty][The second noun][else][regarding the second noun][They][end if] [fill description of the second noun][paragraph break]".

Volume One - New Stuff

Book I - Potion Mixing

Chapter 1 - Tables

Table of Liquids (continued) [First, the ingredients.]
liquid	potable
feather extract	false
salt water	false
grue milk	false
rotgrub hair	false
granola	false
crystal extract	false

Table of Liquids (continued) [Then, the intermediate stages.]
liquid	potable
murky greenish potion	false [For the violet potion.]
watery black potion	false [For the red potion.]
thick orange potion	false [Ditto.]

Table of Liquids (continued) [Now, the potions.]
liquid	potable	flavor
unknown potion	false	--
violet potion	true	"It tastes like exceptionally sour lemon."
ruby red potion	true	"The taste reminds you of cold mint."

Understand "violet" or "potion" as violet potion.
Understand "ruby" or "red" or "potion" as ruby red potion.

Table of Potion Mixtures
primary ingredient	secondary ingredient	ratio	result
feather extract	salt water	0.25	murky greenish potion
grue milk	murky greenish potion	1.5	violet potion
rotgrub hair	crystal extract	5	watery black potion
granola	watery black potion	1.5	thick orange potion
water	thick orange potion	1.4	ruby red potion

Chapter 2 - Mixing Rules

[Based on the example "Exactitude" supplied with the extension.]

The liquid-mixing rulebook has a real number called the recipe ratio.

[Workaround for Inform Bug #0001417]
To decide what real number is the ratio of (V1 - a volume) to (V2 - a volume): (- REAL_NUMBER_TY_Divide({V1}, {V2}) -).

A liquid-mixing rule for a volume (called the poured volume) (this is the assign recipe ratio rule):
	if entry 1 in the recipe contents is the liquid of the noun:
		now the recipe ratio is the ratio of the poured volume to the fluid content of the second noun;
	otherwise:
		now the recipe ratio is the ratio of the fluid content of the second noun to the poured volume;

A liquid-mixing rule for a volume (this is the ensure correct recipe rule):
	let the ingredients be a list of liquids;
	let the amounts be a list of volumes;
	repeat through the Table of Potion Mixtures:
		now the ingredients are {};
		add the primary ingredient entry to the ingredients;
		add the secondary ingredient entry to the ingredients;
		if the ingredients are the recipe contents and the recipe ratio is the ratio entry:
			rule succeeds with result result entry;
	randomize the unknown potion type of the destination container;
	rule succeeds with result unknown potion.

The keep the same recipe as before rule does nothing. The create mixtures rule does nothing. The default conversion of liquids rule does nothing.

The assign recipe ratio rule is listed before the ensure correct recipe rule in the liquid-mixing rulebook.

Chapter 3 - Unknown Potion

The describe empty containers rule response (C) is "[regarding the sample cup][contain] only a trace of [describe the liquid of the sample cup via the sample cup] at the bottom.[no line break]".
The describe graduated containers rule response (A) is "[regarding the sample cup][contain] [the fluid content of the sample cup] of [describe the liquid of the sample cup via the sample cup].[no line break]".
The describe ungraduated containers rule response (F) is " of [describe the liquid of the sample cup via the sample cup].[no line break]".
The suffix with contents rule response (A) is " of [describe the liquid of the target via the target]".
The plural suffix with contents rule response (A) is " of [describe the liquid of the target via the target]".

A fluid container has some text called the unknown potion viscosity. A fluid container has some text called the unknown potion color.
To randomize the unknown potion type of (sample cup - a fluid container):
	if a random number between 1 and 10 is:
		-- 1: now the unknown potion viscosity of the sample cup is "sticky";
		-- 2: now the unknown potion viscosity of the sample cup is "thick";
		-- 3: now the unknown potion viscosity of the sample cup is "watery";
		-- 4: now the unknown potion viscosity of the sample cup is "thin";
		-- 5: now the unknown potion viscosity of the sample cup is "viscous";
		-- 6: now the unknown potion viscosity of the sample cup is "pale";
		-- 7: now the unknown potion viscosity of the sample cup is "dark";
		-- 8: now the unknown potion viscosity of the sample cup is "streaky";
		-- 9: now the unknown potion viscosity of the sample cup is "lumpy";
		-- 10: now the unknown potion viscosity of the sample cup is "creamy";
	if a random number between 1 and 20 is:
		-- 1: now the unknown potion color of the sample cup is "red";
		-- 2: now the unknown potion color of the sample cup is "reddish";
		-- 3: now the unknown potion color of the sample cup is "orange";
		-- 4: now the unknown potion color of the sample cup is "orangish";
		-- 5: now the unknown potion color of the sample cup is "yellow";
		-- 6: now the unknown potion color of the sample cup is "yellowish";
		-- 7: now the unknown potion color of the sample cup is "green";
		-- 8: now the unknown potion color of the sample cup is "greenish";
		-- 9: now the unknown potion color of the sample cup is "blue";
		-- 10: now the unknown potion color of the sample cup is "bluish";
		-- 11: now the unknown potion color of the sample cup is "white";
		-- 12: now the unknown potion color of the sample cup is "whitish";
		-- 13: now the unknown potion color of the sample cup is "off-white";
		-- 14: now the unknown potion color of the sample cup is "gray";
		-- 15: now the unknown potion color of the sample cup is "grayish";
		-- 16: now the unknown potion color of the sample cup is "brown";
		-- 17: now the unknown potion color of the sample cup is "brownish";
		-- 18: now the unknown potion color of the sample cup is "black";
		-- 19: now the unknown potion color of the sample cup is "clear";
		-- 20: now the unknown potion color of the sample cup is "beige".

To say describe (fluid - a liquid) via (sample cup - a fluid container):
	if the fluid is not unknown potion, say "[fluid]";
	otherwise say "[unknown potion viscosity of the sample cup] [unknown potion color of the sample cup] potion".
Understand the unknown potion viscosity property as describing a fluid container when the liquid of the item described is unknown potion.
Understand the unknown potion color property as describing a fluid container when the liquid of the item described is unknown potion.

Volume Two - Scenario

The Laboratory is a room. "The first impression of this room is a sort of controlled chaos. It resembles one of the larger classrooms in Niktolp Hall, but rather than tables and chairs for the students, it is filled with all sorts of strange equipment. "

The Potions Lab is a room. A flask is here. The flask is a fluid container with fluid capacity 50z. A vial is here. The vial is a fluid container with fluid capacity 30z. The player is in the Potions Lab.

In the lab is a liquid stream called a blue fountain. The liquid of the blue fountain is water.
In the lab is a liquid stream called a green fountain. The liquid of the green fountain is salt water.

In the lab is a liquid stream called a white spigot. The liquid of the white spigot is feather extract.
In the lab is a liquid stream called a black spigot. The liquid of the black spigot is grue milk.

]

Include Typographical Conveniences by Daniel Stelzer.
Include Text Capture by Eric Eve.

Volume Zero - Will Be Replaced

A recipe is a kind of value. Some recipes are defined by the Table of Recipes.

Table of Recipes
recipe	color (text)	taste (text)	cooking time (number)	duration (number)	description (text)
null-recipe	--	--	--	--	"error"
fierdno	"violet"	"exceptionally sour lemon"	3	5	"become immune to the venom of small animals"
ondreif	"deep red"	"cool mint"	2	10	"ignore the effects of heat and cold"

Effect is a rulebook with default success.

Copying it to is an action applying to one visible thing and one thing. Understand "copy [something] to [something]" as copying it to.

Volume One - Potions

Book I - Potion Objects

Section A - Potions Themselves

A potion is a kind of thing. Understand "vial" or "glass" or "container" or "flask" as a potion. A potion has a recipe called the enchantment. The description of a potion is usually "[potion-description for the item described].".

To say potion-description for (liquid - a potion):
	let the magic be the enchantment of the liquid;
	say "A small glass vial filled with [color of the magic] liquid. It's labelled '[i][magic] potion - [description of the magic][/i]'".

Section B - Recipe Sheets

A recipe sheet is a kind of thing. A recipe sheet has a recipe called the printing. The description of a recipe sheet is usually "[recipe-description for the item described].".
Definition: a recipe sheet is blank if its printing is the null-recipe.

To say recipe-description for (sheet - a recipe sheet):
	if the sheet is blank:
		say "A blank sheet with spaces to write out an alchemical recipe";
	otherwise:
		let the magic be the printing of the sheet;
		let cap-magic be "[magic]" in upper case;
		say "It's a potion recipe written out in the standard format, as though it were a spell.[p][tt]RECIPE : [cap-magic][br]EFFECT : [description of the magic][br]LASTS  : [duration of the magic] min[br]REQ    : [list of things which comprise the magic][br]TIME   : [cooking time of the magic] min[/tt][br]...and so on".

Book II - Relations

Chapter 1 - Composition

Section A - Definition and Ingredients

Composition relates various things to various recipes. The verb to comprise means the composition relation. The verb to be comprised of means the reversed composition relation.

A common ingredient is a kind of thing. Some common ingredients are defined by the Table of Available Common Ingredients. After printing the name of a common ingredient: say " (common)".

Table of Available Common Ingredients
common ingredient
salt water
pure water
ice
aqua vitae
rock salt

The player carries some feathers, some spider venom, and some grue hair. 

Section B - Layout

Fierdno is comprised of salt water and feathers.
Ondreif is comprised of pure water, rock salt, aqua vitae, spider venom, and grue hair.

Book III - Actions

Chapter 1 - Real Actions

Section A - Drinking

The block drinking rule is not listed in the check drinking rulebook.

Check an actor drinking something (this is the block other people drinking rule): stop the action. [Should probably make this actually work...]

Check drinking something edible (this is the redirect drinking to eating rule): try eating the noun instead.

Check drinking something which is not a potion (this is the can't drink random objects rule):
	say "[The noun] [don't] seem particularly appetizing." (A) instead.

The drinking action has some text called the captured quaffing description. [This buffer holds anything printed by "effect of quaffing" rules. We want to run these rules in the "carry out" stage, but not print their output until the "report" stage. So that output is captured and stored here in the interim.]

Carry out drinking a potion (this is the default drinking rule):
	start capturing text;
	try quaffing the enchantment of the noun;
	stop capturing text;
	now the captured quaffing description is the substituted form of "[captured text]";
	[now the player is affected by the enchantment of the noun.]

Report drinking a potion (this is the describe drinking rule):
	let the magic be the enchantment of the noun;
	say "[We] [swallow] the [color of the magic] liquid in one gulp, and discard the vial. It tastes of [taste of the magic]. ";
	say the captured quaffing description.
To swallow is a verb.

Section B - Copying Recipes

Check copying a recipe sheet to something which is not a recipe sheet: say "Your Alchemy 101 professor drilled into you the importance of keeping recipes on proper recipe sheets[--]a recipe written on [a second noun] would sink your grade deep into the negatives." instead.

Check copying a recipe sheet to a recipe sheet when the second noun is not blank: say "[The second noun] already [have] a recipe written on [them]." instead.
Check copying a blank recipe sheet to a recipe sheet: say "There is nothing on [the noun] to copy yet." instead.

Carry out copying a recipe sheet to a blank recipe sheet: now the printing of the second noun is the printing of the noun.

Report copying a recipe sheet to a recipe sheet: say "[We] meticulously [copy] the recipe from [the noun] to [the second noun]."
To copy is a verb.

Chapter 2 - Fake Fake Actions

Section A - Quaffing

Quaffing is an action applying to one recipe. [This is a "fake fake action" in I6 parlance, since it has an action routine but no grammar lines: it's only triggered by "try".]
Carry out quaffing a recipe:
	follow the effect rules.
Last effect of quaffing:
	say "But nothing seems to happen. Disappointing." [This is much much more likely to happen with spells than potions, but the rule should be provided in case of implementation failures.]

Volume Two - Places



Lab is a room. A red sheet is a recipe sheet in the lab. The printing of the red sheet is fierdno. A blue sheet is a recipe sheet in the lab. The printing of the blue sheet is ondreif. A green sheet is a recipe sheet in the lab.