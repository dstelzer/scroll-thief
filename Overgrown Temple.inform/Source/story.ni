"Temple" by "Daniel M. Stelzer"

Include Dynamic Rooms by Aaron Reed.
Include Implicit Actions by Eric Eve.
Include Typographical Conveniences by Daniel Stelzer.
Include Automap by Mark Tilford.
Include Throwing Between Rooms by Daniel Stelzer.

Use American dialect and the serial comma.

Part I - Aboveground

Section A - Geography from Trizbort

There is a region called the Temple Environs.

There is a room called the Overgrown Temple. "You are standing in the middle of a deep jungle. The screeches of animals, the roar of rushing water, and the fluttering of the wind through the trees all blend together into a vibrant roar of sound. Before you looms a great temple of stone, a proud monument from centuries past. But the jungle has begun to reclaim it, vines climbing across the walls and tree roots pushing the great stone blocks apart. A tunnel to the east still leads inside." It is in Temple Environs.

There is a room called the Inner Temple. "The sounds of the jungle are fainter here, the air cool and damp. This would once have been an antechamber of sorts for the shrine to the east. But the entire wall to the south has collapsed outward, the stones creating a rough clearing amid the trees." It is in Temple Environs.

There is a room called the Shrine. "The great stone altar in the center of the room marks this as the heart of the temple, a ruined shrine to a forgotten god. Grasses poke up through the floor and the altar is bedecked with vines. The statues and idols once adorning the walls are no more than shattered bits of stone, and the bats roosting in the corners are the only worshippers now."[ [if it is sunrise]Blazing sun[else if it is sunset]Dim red [else if it is day]Sun[else if it is night]Moon[end if]light streams in through a gap in the eastern wall, giving a slight touch of life to the ancient shrine."] It is in Temple Environs.

There is a room called the Sinkhole. "A deep cenote has opened in the ground here, swallowing the stones and plants alike. The building stones have shattered, forming crazy jagged shapes all around, and the trees lean inward to almost blot out the sky. At the bottom of the hole the soil abruptly gives away to shiny black stone. A  narrow tunnel has been bored straight downward, perhaps by magic, and a narrow stream of water swirls down into the earth." It is in Temple Environs.

There is a room called the Plaza. "The remains of the temple wall have fallen outward here, the stone blocks scattering across the forest floor and crushing a wide path through the vegetation. The result is a rough [']floor['] divided by deep cracks, and a momentary glimpse of sky. The plants are already working now to reclaim what they have lost. Climbing vines zigzag across the stones, and a few small trees have taken root in the gaps between the blocks.

The stones seem to sink downward off to the southeast." It is in Temple Environs.

There is a dynamic room called the Lair. "The tunnel opens out here into a roughly spherical chamber. The walls are shiny and dark and feel almost like glass to the touch." It is dark. It is in the Obsidian Maze.
When play begins: remove the Lair from the unformed room list.

East of Overgrown Temple is Inner Temple.
South of Inner Temple is Plaza.
East of Inner Temple is Shrine.
Northwest of Sinkhole is Plaza.
Below Sinkhole is Lair.

Section B - Backdrops and Scenery

Instead of listening to an object when the location is in the Temple Environs:
	say "Screeching birds, howling monkeys, rushing water, rustling branches, a lively cacophony."
The ambient jungle noise is a [distant] backdrop in the Temple Environs. Understand "bird" or "birds" or "monkey" or "monkeys" or "animals" or "wind" as the ambient jungle noise. The description of the ambient jungle noise is "You can't see them, only hear them."

Some vines are a backdrop in the Temple Environs. Understand "vine" as the vines. The description of the vines is "Green and leafy."

Some trees are a backdrop in the Temple Environs. Understand "tree" or "root" or "roots" as the trees. The description of the trees is "They are remarkably tenacious, digging into any little bit of soil between the stones of the temple."

Some stone blocks are a backdrop in the Temple Environs. Understand "of stone" or "block" or "great" or "walls" or "wall" as the stone blocks. The description of the stone blocks is "Each block forms a cube almost as tall as you."

A narrow stream of water is [a liquid stream] scenery in the Sinkhole. The description of the stream is "It trickles down into the sinkhole and is lost to sight."

Some statues are scenery in the Shrine. The description of the statues is "They are worn beyond recognition." Understand "statue" or "idol" or "idols" or "carving" or "carvings" or "scene" or "scenes" or "runes" as the statues.

Section C - Altar Puzzle

The great stone altar is fixed in place in the Shrine. The description of the great stone altar is "It was once carved with elaborate scenes and runes of some sort, but they have long since rubbed away."
Rule for writing a paragraph about the altar: now the altar is mentioned. [Making it scenery prevents the display of the scroll.]

On the altar is a crumbling scroll. [It is a spell scroll. The inscribed spell of the crumbling scroll is exex.] "Atop the altar rests a crumbling scroll covered in spiderwebs, a final offering to the gods."
Some spiderwebs are scenery on the altar. Understand "web" or "webs" or "spider webs" or "spiderweb" or "spider web" as the spiderwebs. The description of the spiderwebs is "They cover the altar[if the crumbling scroll is not handled] and its contents[end if] completely."
Some spiders are an animal in the Shrine. "A great multitude of greenish spiders swarms across the altar and the floor." Understand "multitude" or "green" or "greenish" or "spider" or "poisonous" as the spiders. The description of the spiders is "They look poisonous."

The crumbling scroll can be safe or unsafe. The scroll is unsafe.

Part II - Belowground

Chapter 1 - Mazes

There is a region called the Obsidian Maze.

Some glassy stone is a backdrop in the Obsidian Maze. Understand "wall" or "walls" or "glossy" or "glass" or "black" or "rock" or "ceiling" or "floor" or "tunnel" or "tunnels" or "hole" or "holes" or "transparent" as the glassy stone. The description of the glassy stone is "It is very shiny and nearly transparent." The glassy stone is in the Sinkhole.

Section Z - Copied from Scroll Thief

Pointing it at is an action applying to one thing and one visible thing. Understand "point [something preferably held] toward/towards/at/through/into [something]" as pointing it at. Understand "aim [something preferably held] at/through/toward/towards/into [something]" as pointing it at. Understand "point [something preferably held] [direction]" as pointing it at. Understand "aim [something preferably held] [direction]" as pointing it at.

Precondition for pointing a portable not carried not worn thing at something (this is the implicitly take before pointing rule):
	carry out the implicitly taking activity with the noun;
	if the noun is not carried and implicit action attempted is true, stop the action.

Check someone pointing a portable thing at something (this is the npc implicitly take before pointing rule):
	if the actor does not carry the noun and the actor does not wear the noun, try the actor taking the noun;
	if the actor does not carry the noun and the actor does not wear the noun, stop the action.

Report an actor pointing something at an object (this is the standard report pointing rule):
	say "[The actor] [point] [the noun] [if the second noun is not a direction]at [the second noun][else][second noun][end if]."
To point is a verb.

Section A - Tunnels

There are 10 dynamic rooms in the Obsidian Maze.
Definition: a room is volcanic if it is a dynamic room.

When play begins: locate the Lair at xyz { 0, 0, 0 }. The maximum-map-z is 0.

To have (subject - a person) excavate a tunnel to (way - a direction) from (place - a room):
	if the way is inside or the way is outside, stop; [Just a precaution.]
	let notable be whether or not the player is the subject or the player can see the subject;
	if the place is the Sinkhole and the way is down:
		if notable is true, say "[The subject] [point] the staff at the ground, and a wave of force pushes its way through to the Lair.";
		change the down exit of the Sinkhole to the Lair;
		change the up exit of the Lair to the Sinkhole;
		stop;
	let the previous total be available dynamic rooms;
	if notable is true, say "[The subject] [point] the staff [way]ward";
	[First, see if something is stopping us from tunneling.]
	if the place is not volcanic: [Not in the caves.]
		say " but nothing seems to happen.";
		stop;
	if the room way from the place is a room: [Already a tunnel.]
		if notable is true, say ", and the tunnel in that direction seems to glow for a moment.";
		stop;
	if the place is the Lair and the way is up:
		if notable is true, say ", and light shines in as the tunnel to the surface reopens.";
		change the down exit of the Sinkhole to the Lair;
		change the up exit of the Lair to the Sinkhole;
		stop;
	[Now dig a tunnel and report the result.]
	let the excavation be a newly created positioned room way of the place with name "Glassy Tunnel";
	if the rule failed: [Something went wrong.]
		if out of dynamic rooms: [No more power.]
			if notable is true, say ", but it grows hot in [regarding the subject][their] hand and nothing happens.";
		otherwise: [Edge of the map.]
			if notable is true, say ". The rock glows for a moment and begins to flow, but before long the glassy substance gives way to a dull white stone, and the tunnel ends.";
		stop;
	if available dynamic rooms is less than the previous total: [We created something!]
		if notable is true, say ". It vibrates for a moment, humming quietly. A circular outline appears faintly on the stone, then begins to glow red, then white. When the brightness is almost unbearable the stone suddenly shifts, a long circular tunnel punching forward in a spray of glowing droplets. A moment later there is another smooth, glassy tunnel leading [way].";
		now the excavation is unsearched;
		stop;
	[If we got here, we must have hit another room.]
	if notable is true, say ". A circular outline appears in the stone and the tunnel begins to form, bursting into the next room in a spray of glowing droplets."

To have (subject - a person) close the tunnel to (way - a direction) from (place - a room):
	if the way is inside or the way is outside, stop; [Just a precaution.]
	let notable be whether or not the player is the subject or the player can see the subject;
	let the destination be the room way from the place;
	if notable is true, say "[The subject] [point] the staff [way]ward, ";
	if the destination is nothing or (the destination is not volcanic and the destination is not the Sinkhole): [No tunnel to close.]
		if notable is true, say "but nothing seems to happen.";
		stop;
	if notable is true, say "and the rock flows over the hole, covering the tunnel with a barrier of stone.";
	change the way exit of the place to nothing;
	change the (opposite of the way) exit of the destination to nothing.

To have (subject - a person) seal (way - a direction) from (place - a room):
	if the way is inside or the way is outside, stop; [Just a precaution.]
	let notable be whether or not the player is the subject or the player can see the subject;
	let the destination be the room way from the place;
	if notable is true, say "[The subject] [point] the staff [way]ward. ";
	if the destination is not volcanic: [No place to seal.]
		if notable is true, say "Nothing much seems to happen.";
		stop;
	if the destination is the Lair: [Too big.]
		if notable is true, say "The stone seems to flow a bit, but the dornbeast has scratched out a sizeable lair for itself, and it's too big for the staff to collapse.";
		stop;
	if the location is the destination: [The player's in there!]
		say "Stone suddenly begins to flow around you, the floor and the ceiling compressing together as the tunnel seals itself! In a moment you are completely encased in glassy rock, which is a poor substitute for air.";
		[record "Entombed in a sealed tunnel" as an ending;]
		end the story saying "You have died";
		stop;
	if notable is true:
		say "The tunnel in that direction slowly collapses inward, sealing itself again in solid rock.";
		if there is something in the destination:
			say "[The list of things in the destination] probably didn't fare too well.";
	now everything in the destination is nowhere;
	now the destination is not bloody;
	unmap the destination;
	dissolve the destination.

Section B - Descriptions

To say (way - a direction) in title case:
	let T be "[way]";
	say "[T in title case]".

To describe the/-- dynamic exits of (place - a room):
	let the ways be a list of directions;
	let the name text be "";
	let the description text be "";
	[First, construct a list of viable directions.]
	repeat with the way running through directions:
		if the room way from the place is not nothing, add the way to the ways;
	[Now look for correspondences.]
	if the number of entries in the ways is 0: [No exits: bubble.]
		now the name text is "Bubble";
		now the description text is "This is a small spherical chamber, cut off from all other passages.";
	else if the number of entries in the ways is 1: [Only 1 exit: hole, dome, or dead end.]
		let the way be entry 1 in the ways;
		if the way is up:
			now the name text is "Hole";
			now the description text is "This is the bottom of the vertical shaft.";
		else if the way is down:
			now the name text is "Dome";
			now the description text is "This is the top of the vertical shaft.";
		otherwise:
			now the name text is "Dead End";
			now the description text is "The tunnel stops here. The only way out is to the [way].";
	else if the number of entries in the ways is 2: [Only 2 exits: shaft, corridor, or bend.]
		let the primary be entry 1 in the ways;
		let the secondary be entry 2 in the ways;
		if the primary is the opposite of the secondary:
			if the primary is up or the primary is down:
				now the name text is "Shaft";
				now the description text is "This is a straight vertical shaft, with enough handholds in the wall to allow climbing up and down.";
			otherwise:
				now the name text is "[Primary in title case]-[Secondary in title case] Corridor";
				now the description text is "This is a narrow tunnel going straight from the [primary] to the [secondary].";
		otherwise:
			if the secondary is up or the secondary is down: [Swap the variables so the vertical direction is primary.]
				let the temporary be the primary;
				now the primary is the secondary;
				now the secondary is the temporary;
			if the primary is up:
				now the name text is "Bottom of Shaft";
				now the description text is "A steep shaft ends here, curving into a flat corridor to the [secondary].";
			otherwise if the primary is down:
				now the name text is "Top of Shaft";
				now the description text is "A flat tunnel leading off to the [secondary] ends here at a wide hole in the floor.";
			otherwise:
				now the name text is "[Primary in title case]-[Secondary in title case] Bend";
				now the description text is "The tunnel bends here, leading off to the [primary] and [secondary].";
	otherwise: [3+ exits: opening in shaft, crossing, T-intersection, complex junction, complex intersection]
		if up is listed in the ways and down is listed in the ways:
			remove up from the ways;
			remove down from the ways;
			now the name text is "[if the number of entries in the ways is one][entry one in the ways in title case] [end if]Opening in Shaft";
			now the description is "[if the number of entries in the ways is greater than one]Passages lead[else]A passage leads[end if] off to the [ways] from the middle of a vertical shaft.";
		otherwise:
			let the count be the number of entries in the ways;
			let the pairs be a list of directions;
			let L be the ways;
			repeat with the way running through L:
				let the reverse be the opposite of the way;
				if the reverse is listed in the ways:
					add the way to the pairs;
					remove the way from the ways;
					remove the reverse from the ways;
			let the passages be a list of text;
			repeat with the way running through the pairs:
				add the substituted form of "[way]-[opposite of the way]" to the passages;
			if the number of entries in the ways is zero:
				now the name text is "Crossing";
				now the description text is "This is a wide intersection of passages running [passages].";
			otherwise if the number of entries in the pairs is one:
				now the name text is "T-Intersection";
				now the description text is "[if the number of entries in the ways is 1]A passage [entry 1 in the ways] splits[else]Passages [ways] separate[end if] off a longer [entry 1 in the passages] tunnel here.";
			otherwise if the number of entries in the pairs is zero:
				now the name text is "Complex Junction";
				now the description text is "Passages from the [ways] all come together here in a wide junction.";
			otherwise:
				now the name text is "Complex Intersection";
				now the description text is "[if the number of entries in the ways is 1]A single passage [entry 1 in the ways] separates[else]Single passages [ways] split[end if] off from the intersection of the [passages] tunnels.";
	[if the place is the Lair:
		now the name header is "Lair";
	otherwise:
		now the name header is "Glassy Tunnel";]
	now the printed name of the place is the substituted form of "[Name Text]";
	now the description of the place is the substituted form of "[description text]".

Before looking when the location is a dynamic room and the location is not the Lair: describe the dynamic exits of the location.
Before going to a dynamic room (called the place): unless the place is the Lair, describe the dynamic exits of the place.

After looking when the location is the Lair:
	let the ways be a list of directions;
	repeat with the way running through directions:
		if the room way from the Lair is not nothing, add the way to the ways;
	if the number of entries in the ways is greater than one:
		say "Tunnels lead [ways].";
	continue the action.

Section C - The Staff

The Staff of Cunicular Excavation is carried by the player. The staff can be gold-tipped, silver-tipped, or copper-tipped (this is its mode). The staff is copper-tipped. After printing the name of the [examined] Staff while taking inventory: say " ([mode])". The description of the Staff is "It's a narrow rod of twisted black iron tipped with shiny [if the staff is gold-tipped]gold[else if the staff is silver-tipped]silver[else]copper[end if], a bit longer than you are tall. Inscribed down the side are the words [i]Staff of Cunicular Excavation - [if the staff is gold-tipped]close[else if the staff is silver-tipped]collapse[else]bore[end if] a tunnel through volcanic glass[/i]. A grip in the middle looks like it could be turned." Understand "twisted" or "black" or "iron" or "narrow" or "rod" or "shiny" or "grip" or "tip" or "tipped" as the Staff. Understand "gold" or "golden" as the staff when the staff is gold-tipped. Understand "silver" as the staff when the staff is silver-tipped. Understand "copper" as the staff when the staff is copper-tipped. Understand the mode property as describing the staff. The indefinite article of the staff is "the".
The staff is lit.

Instead of an actor turning the staff:
	if the staff is gold-tipped:
		try the actor setting the staff to "silver";
	else if the staff is silver-tipped:
		try the actor setting the staff to "copper";
	otherwise:
		try the actor setting the staff to "gold".
To twist is a verb.

Understand "turn [something] to [text]" as setting it to.
Instead of an actor setting the staff to a topic listed in the Table of Staff Settings:
	if the result entry is:
		-- 1: now the staff is gold-tipped;
		-- 2: now the staff is silver-tipped;
		-- 3: now the staff is copper-tipped;
	if the actor is the player or the player can see the actor, say "[The actor] [twist] the grip, and the point shines for a moment. The words on the side seem to flow and change."

Table of Staff Settings
topic	result
"gold/golden tip/tipped/--" or "close/seal"	1
"silver tip/tipped/--" or "collapse"	2
"copper tip/tipped-/--" or "open"	3

The staff time warning flag is initially false.
Instead of an actor pointing the staff at a direction:
	[take full time;]
	if the dornbeast is awake and the location is volcanic: [Arranging passages can take a long time.]
		follow the dornbeast movement rules;
		if the story has ended, stop the action;
		follow the dornbeast movement rules;
		if the story has ended, stop the action;
	otherwise if the player is the actor and the staff time warning flag is false:
		say "You concentrate on the staff, and after a minute or two you feel the power awaken within it. It doesn't take long, but in a life or death situation that time might be costly.[p]";
		now the staff time warning flag is true;
	if the staff is gold-tipped:
		have the actor close the tunnel to the second noun from the location of the actor;
	else if the staff is silver-tipped:
		have the actor seal the second noun from the location of the actor;
	otherwise:
		have the actor excavate a tunnel to the second noun from the location of the actor;
	if the location of the actor is the location, try looking.

Tunnelling is an action applying to one visible thing. Understand "tunnel [direction]" or "open [direction]" or "burrow [direction]" as tunnelling.
Check tunnelling when the player does not carry the Staff:
	if the player can touch the Staff:
		carry out the implicitly taking activity with the staff;
		if the player carries the staff, continue the action;
	say "You don't have any good way to do that." instead.
Check someone tunnelling when the actor does not carry the Staff (this is the can't tunnel without the staff rule): stop the action.
Carry out an actor tunnelling:
	unless the staff is copper-tipped:
		if the actor is the player:
			say "(first turning the staff)[ccb]";
			silently try setting the staff to "copper";
		otherwise:
			try the actor setting the staff to "copper";
	try the actor pointing the staff at the noun.

Collapsing is an action applying to one visible thing. Understand "collapse [direction]" as collapsing.
Check collapsing when the player does not carry the Staff:
	if the player can touch the Staff:
		carry out the implicitly taking activity with the staff;
		if the player carries the staff, continue the action;
	say "You don't have any good way to do that." instead.
Check someone collapsing when the actor does not carry the Staff (this is the can't collapse without the staff rule): stop the action.
Carry out an actor collapsing:
	unless the staff is silver-tipped:
		if the actor is the player:
			say "(first turning the staff)[ccb]";
			silently try setting the staff to "silver";
		otherwise:
			try the actor setting the staff to "silver";
	try the actor pointing the staff at the noun.

Sealing is an action applying to one visible thing. Understand "seal [direction]" or "close [direction]" as sealing.
Check sealing when the player does not carry the Staff:
	if the player can touch the Staff:
		carry out the implicitly taking activity with the staff;
		if the player carries the staff, continue the action;
	say "You don't have any good way to do that." instead.
Check someone sealing when the actor does not carry the Staff (this is the can't seal without the staff rule): stop the action.
Carry out an actor sealing:
	unless the staff is gold-tipped:
		if the actor is the player:
			say "(first turning the staff)[ccb]";
			silently try setting the staff to "gold";
		otherwise:
			try the actor setting the staff to "gold";
	try the actor pointing the staff at the noun.

Chapter 2 - Dornbeasts

Section A - The Dornbeast Itself

A dornbeast is an animal in the Lair. The dornbeast can be asleep or awake. The dornbeast is asleep. "A massive dornbeast [if asleep]lies here, sound asleep[else]is pacing the room, searching for its prey[end if]." The description of the dornbeast is "Its lower body is covered in hard green scales, while its upper body is purple and blue with a great number of eyes facing in all directions. This monster is easily recognizable as a deadly dornbeast[--][if asleep]thankfully, a sleeping one.[else]and it seems to be hunting for you!" Understand "dorn" or "beast" or "deadly" as the dornbeast.

[Casting sinpse is an action applying to nothing. Understand "sinpse" as casting sinpse.
Carry out casting sinpse:
	say "The tunnel collapses!";
	change the down exit of the Sinkhole to nothing;
	change the up exit of the Lair to nothing;
	now the dornbeast is awake.]

The player carries an Elixir of Life. The description of the Elixir of Life is "A small folded packet the size of a teabag, dyed a violent red. Instructions are printed on one side in fluorescent green, clashing horribly with the color of the paper:[p][tt]~~ ELIXIR OF LIFE Study Aid ~~[br]Can't stay awake in class? Spent half the night studying for an exam? Forget about setting an alarm! Simply throw an ELIXIR OF LIFE* packet against a hard surface, and everyone in the vicinity** will be wide awake in seconds![br][br]* Registered trademark of the Frobozz Magic Stimulant Company[br]** Only useful in enclosed*** spaces[br]*** Hermetically sealed[/tt]".
A torn wrapper is a thing. The description of the wrapper is "Faded and empty, the magic is gone now. It seems an Elixir of Life can only be used once."

To set off the Elixir of Life:
	let notable be whether or not the player can see the Elixir of Life;
	let the place be the location of the Elixir of Life;	
	move the torn wrapper to the holder of the Elixir of Life;
	remove the Elixir of Life from play;
	if notable is true, say "The packet hits the ground and begins puffing up, inflating until it's nearly the size of a football. Then it bursts open with a [i]bang[/i] and the air is filled with a mist of bright-red particles. ";
	if the place is not volcanic or the number of moves from the place to the Sinkhole is not -1:
		if notable is true, say "But [the place] must not be hermetically sealed, because the cloud is whipped away almost instantly by some slight draft, leaving only a torn wrapper on the ground.";
		stop;
	if the location is the place:
		say "The burning smell of cinnamon and cloves leaves you coughing as the cloud spreads itself throughout the room.";
	otherwise:
		let the way be the best route from the location to the place;
		if the way is not nothing:
			say "The cloud spreads quickly. In seconds it rolls in from [the way], stinging your nose with a strong smell of cinnamon and cloves.";
	if the the dornbeast is asleep:
		if the number of moves from the place to the Lair is -1:
			if the location is volcanic, say "You wait a moment, but nothing else happens. It seems the mist didn't reach the dornbeast.";
		otherwise:
			if the location is volcanic, say "Somewhere nearby, you hear a muffled roar as the dornbeast awakens from its slumber...";
			awaken the dornbeast.

Last after an actor dropping the Elixir of Life:
	set off the Elixir of Life;
	continue the action.
Last after an actor throwing the Elixir of Life at:
	set off the Elixir of Life;
	continue the action.

To awaken the dornbeast:
	[change the down exit of the Sinkhole to nothing;
	change the up exit of the Lair to nothing;]
	now the dornbeast is awake.

There is a featureless white cube in the Lair. Instead of taking the cube when the dornbeast is asleep: say "The dornbeast stirs slightly as you approach, and you think better of it. You don't want to be right next to it if it wakes up."

After taking the cube: end the story finally saying "Yay you win"

[Put spells and stuff here.]

Section B - AI

The dornbeast movement rules are a rulebook.
Definition: a person is prey if she is not the dornbeast.
Definition: a person is other if she is not the player.

A dynamic room can be bloody. A dynamic room is usually not bloody.
The remains of a previous meal are a backdrop. They are not scenery. The initial appearance of the remains is "Spread throughout the room are the remains of...something. You don't want to look too closely." The description of the remains is "Blood, quite a lot of it, and some well-chewed bits of bone."
When play begins: move the remains backdrop to all bloody dynamic rooms.

Every turn when the dornbeast is awake (this is the invoke dornbeast movement rule):
	follow the dornbeast movement rules.
	[let the place be the location of the dornbeast;
	say "Dornbeast location: ( [x-coordinate of the place] [y-coordinate of the place] [z-coordinate of the place] )[p]".]

Dornbeast movement when the dornbeast can touch the player (this is the dornbeast can eat player rule):
	say "The dornbeast, finding you in the vicinity, turns its paralyzing gaze in your direction. You are quickly and unceremoniously devoured.";
	[record "Eaten by a dornbeast" as an ending;]
	end the story saying "You have died";
	rule succeeds.

Dornbeast movement (this is the dornbeast smells adjacent prey rule):
	if the location of the dornbeast encloses prey, make no decision; [Bird in the hand]
	repeat with the way running through directions:
		let the place be the room (way) from the location of the dornbeast;
		if the place is a volcanic room and the place encloses other prey:
			try the dornbeast going way;
			break; [It'll eat in the next rule]
	make no decision.

Dornbeast movement when the dornbeast can touch an other prey person (called the target) (this is the dornbeast eats other prey rule):
	if the player can see the target:
		say "With a hungry roar, the dornbeast pounces on [the target]! You avert your eyes, but the sound is clear enough.";
	otherwise if the location is volcanic:
		let the way be the best route from the location to the location of the target;
		if the way is a direction:
			say "From [the way] comes the sound of a panicked [target], followed by the dornbeast's roar, and then a horrible ripping and chewing sound...";
	now the location of the target is bloody;
	update backdrop positions;
	remove the target from play;
	rule succeeds.

Dornbeast movement (this is the dornbeast defeats scrying rule):
	[repeat with the primary running through active scrying devices which the dornbeast can see:
		let the secondary be the mystical connection of the primary;
		if the player can see the secondary:
			say "[The secondary] [let] out an earsplitting screech. Cracks run across [their] surface, and a wisp of blue smoke escapes.";
		deactivate the primary;
		now the primary is damaged;
		now the secondary is damaged;]
	make no decision.

Dornbeast movement (this is the dornbeast line of sight to player rule):
	let the place be the location of the dornbeast;
	let the way be the best route from the location of the dornbeast to the location;
	if the way is nothing, make no decision;
	let the step be the way;
	while the step is the way:
		let the step be the best route from the place to the location;
		if the step is not the way, make no decision;
		let the place be the room step from the place;
		if the place is the location:
			say "You suddenly feel the strength leaving your limbs. [if the way is up]From below you[else if the way is down]From above you[else]Off to the [opposite of the way][end if] the dornbeast has turned one of its eyes toward you, and you are helpless to escape as it rushes towards its prey...";
			[record "Eaten by a dornbeast" as an ending;]
			end the story saying "You have died";
			rule succeeds.

The scent-tracking flag is initially false.
Dornbeast movement (this is the dornbeast can smell player rule):
	let the distance be the number of moves from the location of the dornbeast to the location;
	if the distance is -1: [No route.]
		now the scent-tracking flag is false;
		make no decision;
	if the distance is greater than 3: [Too far away.]
		if the scent-tracking flag is true, say "You hear a roar of disappointment. It seems the dornbeast has lost your scent.";
		now the scent-tracking flag is false;
		make no decision;
	if the scent-tracking flag is false:
		say "You hear a tremendous 'Hurumph!' as the dornbeast catches your scent in the air!";
		now the scent-tracking flag is true;
	otherwise:
		say "The dornbeast is still following your trail, and seems to be getting closer!";
	let the way be the best route from the location of the dornbeast to the location;
	if the way is a direction, try the dornbeast going the way.

The dornbeast moving twice as fast as the player flag is always false.

Dornbeast movement when the scent-tracking flag is true (this is the dornbeast follows scent rule): [The dornbeast gets to move twice when it's found a trail.]
	[if the dornbeast moving twice as fast as the player flag is true:]
	let the way be the best route from the location of the dornbeast to the location;
	if the way is a direction, try the dornbeast going the way;
	if the dornbeast can touch the player:
		say "The dornbeast bounds into the room[if the way is up] from below[else if the way is down] from above[else if the way is not nothing] from the [opposite of the way][end if], roaring ferociously! Before you can even move it has caught you in its grasp, and the strength leaves your body under the force of its paralyzing gaze...";
		[record "Eaten by a dornbeast" as an ending;]
		end the story saying "You have died";
		rule succeeds.

A dynamic room can be searched or unsearched. A dynamic room is usually unsearched.
A dynamic room has a number called the priority.
The dornbeast has a list of dynamic rooms called the tracking queue.

Dornbeast movement when the scent-tracking flag is false (this is the dornbeast explores rule):
	now the location of the dornbeast is searched;
	remove the location of the dornbeast from the tracking queue of the dornbeast, if present;
	repeat with the place running through unsearched rooms adjacent to the location of the dornbeast:
		add the place to the tracking queue of the dornbeast, if absent;
	let L be the tracking queue of the dornbeast; [Make a copy first so we can edit it.]
	repeat with the place running through L:
		now the priority of the place is the number of moves from the location of the dornbeast to the place;
		if the priority of the place is -1, remove the place from the tracking queue of the dornbeast; [Don't keep trying to find unreachable places.]
	if the tracking queue of the dornbeast is empty: [No options? Start the search again!]
		if the number of moves from the location of the player to the location of the dornbeast is not -1, say "A howl of frustration resounds through the maze as the dornbeast begins to retrace its steps.";
		now every dynamic room is unsearched;
		make no decision;
	sort the tracking queue of the dornbeast in priority order;
	let the destination be entry 1 in the tracking queue of the dornbeast;
	let the way be the best route from the location of the dornbeast to the destination;
	if the way is a direction, try the dornbeast going the way;
	if the number of moves from the location of the player to the location of the dornbeast is not -1:
		say "[one of]Footsteps echo through the caves, seeming to come from every direction at once.[or]Silence for a moment, then a series of high-pitched chirps, then footsteps, faster than before.[or]You can hear the dornbeast's breathing somewhere nearby.[or]Claws against stone, somewhere close, but where?[or]The footsteps again, are they getting closer?[at random]".

[First]
The dornbeast smells adjacent prey rule is listed last in the dornbeast movement rulebook.
The dornbeast eats other prey rule is listed last in the dornbeast movement rulebook.
The dornbeast can eat player rule is listed last in the dornbeast movement rulebook.
The dornbeast line of sight to player rule is listed last in the dornbeast movement rulebook.
The dornbeast defeats scrying rule is listed last in the dornbeast movement rulebook.
The dornbeast can smell player rule is listed last in the dornbeast movement rulebook.
The dornbeast follows scent rule is listed last in the dornbeast movement rulebook.
The dornbeast explores rule is listed last in the dornbeast movement rulebook.
[Last]

Book Last - Debug

When play begins:
	say "Instructions: go down and get the cube without dying. The staff in your inventory will help. You can e.g. OPEN NORTH / CLOSE NORTH / COLLAPSE NORTH to use its magic.".

The player is in the Sinkhole.

The Adventurer is a man in the Sinkhole.
A little bird is an animal in the Sinkhole.
Persuasion: persuasion succeeds.

Every turn when the Adventurer is in a volcanic room and the dornbeast is awake:
	let the next place be a random room which is adjacent to (the location of the Adventurer);
	if the next place is nothing, make no decision;
	let the way be the best route from the location of the Adventurer to the next place;
	try the Adventurer going way.
Every turn when the little bird is in a volcanic room and the dornbeast is awake:
	let the next place be a random room which is adjacent to (the location of the little bird);
	if the next place is nothing, make no decision;
	let the way be the best route from the location of the little bird to the next place;
	try the little bird going way.