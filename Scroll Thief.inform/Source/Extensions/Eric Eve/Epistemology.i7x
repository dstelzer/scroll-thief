Version 7 of Epistemology by Eric Eve begins here.

"Keeping track of what the player character knows and sees."

"modified slightly by Daniel Stelzer"

Book 1 - Sight

A thing can be either seen or unseen. A thing is usually unseen.

[It might seem more straightforward simply to write "Now everything visible in
the location is seen." but this turns out to be unacceptably slow in practice.
The following code does approximately the same thing but much faster.]

Carry out looking (this is the mark items as seen when looking rule): 
	unless in darkness:
		repeat with item running through things that are enclosed by the location:  
			if the item is not enclosed by an opaque closed container:
				now the item is seen.

Carry out opening a container (this is the mark items as seen on opening a container rule):
	repeat with item running through things that are enclosed by the noun:
		if the item is unseen and the item is visible:
			now the item is seen.

The mark items as seen on opening a container rule is listed after the
standard opening rule in the carry out opening rules.

Book 2 - Familiarity

Chapter 1 - Basic Familiarity

A thing can be familiar or unfamiliar. A thing is usually unfamiliar.

Definition: a thing is known if it is familiar or it is seen.

Definition: a thing is unknown if it is not known.

Chapter 2 - Subject (for use without Threaded Conversation by Emily Short) 

A subject is a kind of thing. The specification of a subject is "Something
that conversation can refer to, but which has no real-world presence or
functionality."

Chapter 3 - Familiarity of Subjects

A subject is usually familiar.

Book 3 - Examined-ness

A thing can be examined or unexamined. A thing is usually unexamined.

Carry out examining something visible (this is the mark items as seen on examining rule):
	now the noun is examined;
	now the noun is seen.

Book 3 - Testing commands - not for release

Requesting epistemic status of is an action out of world applying to one visible thing.

Understand "epistat [any thing]" as requesting epistemic status of.

Report requesting epistemic status of (this is the report epistemic status rule):
	say "[noun] - [if seen]seen[otherwise]unseen[end if] /
		[if familiar]familiar[otherwise]unfamiliar[end if] /
		[if examined]examined[otherwise]unexamined[end if] /
		[if known]known[otherwise]unknown[end if]."

Epistemology ends here.

---- DOCUMENTATION ----

This is mostly equivalent to the extension by Eric Eve. It adds the additional flag "examined/unexamined", and sets that instead of "familiar" when examining. That's about it.
