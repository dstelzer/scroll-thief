Throwing Between Rooms by Daniel Stelzer begins here.

[This is partially based on the example "Kyoto", with more rules for things moving between rooms and fewer for things being damaged or destroyed.]

The throwing it at action has an object called the throw destination (matched as "to").
The throwing it at action has an object called the throw source (matched as "from").

The futile to throw things at inanimate objects rule is not listed in any rulebook.

The block throwing at people rule is listed instead of the block throwing at rule in the check throwing it at rules.
This is the block throwing at people rule:
	if the second noun is a person, say "That might be construed as an attack." (A) instead.

Understand "drop [something preferably held] [direction]" as throwing it at.

To sail is a verb. To toss is a verb. To fling is a verb. To disappear is a verb. To collide is a verb. To land is a verb. To come is a verb.

Setting action variables for throwing something at (this is the throwing action variable rule):
	now the throw source is the location of the actor;
	now the throw destination is the second noun;
	if the throw destination is a direction, now the throw destination is the room-or-door second noun from the location of the actor;
	if the throw destination is an open door, now the throw destination is the other side of the throw destination from the location of the actor.

Check an actor throwing something at when the noun is not carried by the actor (this is the can't throw what you don't have rule):
	if the actor is the player:
		carry out the implicitly taking activity with the noun;
		if the noun is carried, continue the action;
	if the actor is the player or the player can see the actor:
		say "[The actor] [aren't] even holding [the noun]." (A);
	stop the action.

Check an actor throwing something at when the second noun is a direction (this is the switch throwing between rooms to throwing at doors rule):
	if the throw destination is a door, try the actor throwing the noun at the throw destination instead.

Check an actor throwing something at to nothing (this is the can't throw to nowhere rule):
	if the player is the actor:
		say "Even with momentum, [the noun] [can't] go that way." (A);
	otherwise if the player can see the actor:
		say "[There] [are] nowhere for [regarding the noun][them] to go in that direction." (B);
	stop the action;

Carry out an actor throwing something at to a room (this is the primary throw between rooms rule):
	move the noun to the throw destination;
	if the player can see the noun and the player cannot see the actor:
		say "[The noun] [come] flying into [the throw destination] from [the opposite of the second noun]." (A).

Report an actor throwing something at a direction (this is the report throwing between rooms rule):
	say "[The actor] [fling] [the noun] as hard as [regarding the actor][they] [can]. [regarding the noun][They] [sail] [second noun]ward and [disappear] into [the location of the noun]." (A).

[Check an actor throwing something at a door (this is the check throwing at doors rule):
	if the second noun is closed:
		if the player can see the actor:
			say "[The noun] [collide] with [the second noun] without much effect." (A);
		move the noun to the location of the actor;
		now the throw destination is the location of the actor;
		stop the action.

Carry out an actor throwing something at a door (this is the second throw between rooms rule):
	let the place be the other side of the second noun from the location of the actor;
	now the throw destination is the place;
	move the noun to the place.

Report an actor throwing something at a door (this is the report throwing at doors rule):
	say "[The noun] [sail] out of sight into [the location of the noun]." (A).]

Last carry out an actor throwing something at something (this is the landing rule):
	let the destination be the location of the actor;
	if the second noun is on a supporter (called the table), let the destination be the table;
	if the second noun is a supporter, let the destination be the second noun;
	move the noun to the destination;
	now the throw destination is the destination.

Report an actor throwing something at something (this is the general report throwing rule):
	say "[The noun] [land] [if the throw destination is the location]nearby[else if the throw destination is a room]in [the throw destination][else]on [the throw destination][end if]." (A).

Throwing Between Rooms ends here.
