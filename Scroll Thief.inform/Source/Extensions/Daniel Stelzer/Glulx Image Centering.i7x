Version 3 of Glulx Image Centering (for Glulx only) by Daniel Stelzer begins here.

"Glulx Image Centering adds the ability to display an image that is centered (left/right) on the screen."

"modified from the extension by Emily Short"

Include Glulx Text Styles by Daniel Stelzer.

Table of User Styles (continued)
style name	justification	obliquity	indentation	first-line indentation	boldness	fixed width	relative size
special-style-1	center-justified	no-obliquity	0	0	regular-weight	proportional-font	0


To display (chosen figure - a figure-name) centered:
	say "[using special-style-1]";
	say " ";
	display chosen figure inline;
	say " [line break]"; 
	say "[using normal style]"; 
	
To display (chosen figure - a figure-name) inline:
	(- DrawInline({chosen figure}); -)

Include (-

[ DrawInline N;
	if(glk_gestalt(gestalt_DrawImage, wintype_TextBuffer) == 1){
		glk_image_draw(gg_mainwin, ResourceIDsOfFigures-->N,  imagealign_InlineUp, 0);
	}else{
		print "[IMAGE ", n, " HERE]";
	}
]; 

-)

Glulx Image Centering ends here.

---- Documentation ----

Glulx Image Centering adds the ability to display an image that is centered (left/right) on the screen. To do this, it uses up the first of the two available custom user text styles, defining it to be center-justified. 

To invoke Glulx Image Centering, we say

	display figure foo centered.

To display cover art in our game, we might include something like this:

	Include Glulx Image Centering by Emily Short. Include Basic Screen Effects by Emily Short.

	When play begins: 
		display figure of small cover centered;
		pause the game.

This would display a picture in the middle of the screen, then wait for a keypress, then clear the screen before going on with the game.

A word of warning: not all Glulx interpreters will necessarily handle this operation correctly. Authors are advised to check the performance of their game on a variety of interpreters, or have their beta-testers do so.

