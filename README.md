# README #

This is the main source code for Scroll Thief, hosted in source control for safety and preservation. Compiling it will require Inform 7 6L38 and a wide variety of extensions, most of which are hosted at [the Extensions Github](https://github.com/I7/extensions). The game file itself can be found [on the IFDB](http://ifdb.tads.org/viewgame?id=o6kvclutag67skou) or [on its own site](http://meadstelzer.com/daniel/if/scrollthief/), along with syntax-highlighted [source](http://meadstelzer.com/daniel/if/scrollthief/source.html).
