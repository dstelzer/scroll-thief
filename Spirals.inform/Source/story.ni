"Spirals" by "Daniel M. Stelzer"

Include Repeat Boxes by Dave Robinson.
Include Typographical Conveniences by Daniel Stelzer.
Include Autosave by Daniel Stelzer.
Include Specific Time by Daniel Stelzer.
Include Modified Timekeeping by Daniel Stelzer.
Include Object Kinds by Brady Garvin.
Include Default Styles by Daniel Stelzer.
Use persistent autosaves.

Definition: a thing is other if it is not the player.
The player standin is a person.

A person is usually lit. [For now, dispense with FROTZ spells.]

Volume - Fugue

Book I - Machinery

A temporal iteration is a kind of value. The temporal iterations are iteration null, iteration alpha, iteration beta, iteration gamma, and iteration delta.
Definition: something (called the item) is temporally unallocated:
	if the item does not provide the property temporal iteration, no;
	if the temporal iteration of the item is iteration null, yes;
	no.
Definition: something is temporal if it provides the property temporal iteration.

Part 1 - Selves

Chapter 1 - Setup

A former self is a kind of person. The printed name of a former self is usually "your twin". A former self is usually proper-named. Understand "your" or "twin" as a former self. A former self has a temporal iteration. A former self has an object called the initialization.
Self alpha is an iteration alpha former self. Self beta is an iteration beta former self. Self gamma is an iteration gamma former self. Self delta is an iteration delta former self. Self epsilon is a former self.

To say a (something - a former self): say "your twin". [Function overloads for different ways to say their name]
To say an (something - a former self): say "your twin".
To say A (something - a former self): say "Your twin".
To say An (something - a former self): say "Your twin".
To say the (something - a former self): say "your twin".
To say The (something - a former self): say "Your twin".

To decide whether (X - a former self) is younger than (Y - a former self): [A relation would be the proper way to implement this, but there are only four and they're constant, so this ends up being more concise.]
	if X is Y, no;
	if Y is self delta, yes;
	if X is self delta, no;
	if X is self alpha, yes;
	if Y is self alpha, no;
	if Y is self gamma, yes;
	no.
To decide whether (X - a former self) is older than (Y - a former self):
	if X is Y, no;
	if X is younger than Y, no;
	yes.

To decide whether time-travelling:
	if the player is a former self, yes;
	no.

Chapter 2 - Recorded Actions and Locations

Table of Time-Travel Actions
alpha (a stored action)	beta (a stored action)	gamma (a stored action)	delta (a stored action)
with 30 blank rows.

Table of Time-Travel Locations
alpha location (an object)	beta location (an object)	gamma location (an object)	delta location (an object)
with 30 blank rows.

The most recent time-travel action is a stored action that varies.
Before doing anything when time-travelling:
	now the most recent time-travel action is the current action.

Chapter 3 - Changing

To silently switch selves to (the new self - a person):
	move the new self to the holder of the player;
	now everything carried by the player is carried by the new self;
	now everything worn by the player is worn by the new self;
	now the player is the new self.

To refresh (the item - a thing) for time-travel:
	if the item is off-stage, stop;
	if the item is not temporal:
		say "ERROR: [the item] cannot be used for time-travel!";
		stop;
	let T be the temporal iteration after the temporal iteration of the item;
	let the replacement be a selection from the object kind of the item for T;
	if the item is a container or the item is a supporter or the item is a person and the first thing held by the item is not nothing:
		repeat with the content running through things held by the item:
			refresh the item for time-travel;
		repeat with the content running through things held by the item: [Need a second loop because the contents have changed!]
			move the item to the replacement;
	move the replacement to the holder of the item;
	now the initialization of the replacement is the holder of the item;
	if the item is the player, now the player is the replacement;
	remove the item from play.

To cause time-travel:
	[if self delta is on-stage, refresh self delta for time travel;]
	refresh self gamma for time-travel;
	refresh self beta for time-travel;
	refresh self alpha for time-travel;
	reset temporal item locations;
	now the time-travel scheduling timer is zero.

Part 2 - Objects

The time travel limit is always four.
The temporal number is always sixteen. [Should be 2**limit]

Chapter 1 - Tracking

A notability state is a kind of value. The notability states are unnotable, notably open, and notably closed. [This indicates that the player noticed the state of the object, e.g. a door being open. If the state is different later, it's a problem.]

Table of Sightings
timestamp (a number)	item (an object)	type (an object kind)	place (an object)	viewer (a former self)	status (a notability state)
with 128 blank rows.
[This table holds:
timestamp - which turn this record is from
item - which object was seen (if type is empty)
type - the kind of the object seen (it item is empty)
place - the holder of the object
viewer - who saw it
status - if the object was open, closed, etc]

To record (subject - a former self) seeing (item - a thing) in (holder - an object) as (status - a notability state) at (tick - a number):
[	say "[t]>> Record [temporal iteration of the subject] seeing [the item][if the item is temporal] ([temporal iteration of the item])[end if] [status] in [the holder] (tick [tick])[br]";]
	choose a blank row from the Table of Sightings;
	now the timestamp entry is the tick;
	if the item is temporal:
		now the type entry is the object kind of the item;
	otherwise:
		now the item entry is the item;
	now the place entry is the holder;
	now the viewer entry is the subject;
	now the status entry is the status.

This is the update object sightings rule:
[	say "[t]Updating sightings[br]";]
	repeat with the item running through visible things:
[		say "[t]>Marking [the item][br]";]
		if the item is the player, next;
		if the item is scenery, next;
		if the item is a backdrop, next;
		let the status be unnotable;
		if the item is an openable container or the item is an openable door:
			if the item is open, let the status be notably open;
			otherwise let the status be notably closed;
		record the player seeing the item in the holder of the item as status at (time-tick).

To reset temporal item locations:
	repeat with the item running through temporal things:
		if the initialization of the item is not nothing:
			move the item to the initialization of the item;
		otherwise:
			remove the item from play.

Chapter 2 - Things

[Things which can be used in time-travel are rather strange. They need to be able to be duplicated at-will. Unfortunately, dynamic object creation requires dynamic tables, which aren't supported in this version of I7. So we create a stock of them to move around as needed.

However, this is a tedious task, and a change in the time-travel code might need to be duplicated between all the objects. So, a quick Python script generates the repetitive parts once I define the object kinds:

#!/usr/bin/env python3

from sys import argv

print('[' + argv[0] + ' ' + argv[1] + ' ' + ' '.join('"{}"'.format(arg) for arg in argv[2:]) + ']' + '\n')

for arg in argv[2:]:
	print('[{}]'.format(arg))
	print('There are {1} {0}s in Temporal Limbo. A {0} has a temporal iteration. A {0} has an object called the initialization.\n'.format(arg, argv[1]))

]

Temporal Limbo is a container.

Section A - Human-Written Code

A waxy scroll is a kind of thing. The description of a waxy scroll is "SNAVIG".

A shimmering book is a kind of thing. The description of a shimmering book is "GOLMAC".

Section B - Machine-Written Code

[./gen.py 16 "waxy scroll" "shimmering book"]

[waxy scroll]
There are 16 waxy scrolls in Temporal Limbo. A waxy scroll has a temporal iteration. A waxy scroll has an object called the initialization.

[shimmering book]
There are 16 shimmering books in Temporal Limbo. A shimmering book has a temporal iteration. A shimmering book has an object called the initialization.

Chapter 3 - Creating

To decide what object is a/the/-- new/-- selection from (K - an object kind) for (T - a temporal iteration):
	repeat with the item running through objects of kind K:
		if the item is not in Temporal Limbo, next;
		if the item is temporal and the temporal iteration of the item is T:
			decide on the item;
		otherwise if the item is temporally unallocated:
			now the temporal iteration of the item is T;
			decide on the item;
	say "[alert]ERROR: no more [K] available![/alert]"

To decide what object is a/the/-- new/-- selection from (D - a description of objects) for (T - a temporal iteration):
	repeat with the item running through D:
		if the item is temporal and the item is in Temporal Limbo and the temporal iteration of the item is T:
			decide on the item;
		otherwise if the item is temporally unallocated:
			now the temporal iteration of the item is T;
			decide on the item;
	say "[alert]ERROR: no more objects-matching-description available![/alert]"

Part 3 - Paradoxes

Chapter 1 - Recording

The File of Paradoxes is called "paradoxcount". [We want to keep track of the number of paradoxes caused to give appropriate messages to the player. But a variable would get wiped during autosave/autorestore. Solution: write the number to an external file, then read it back in.]

Table of Paradoxes [This isn't really a table, just a simple way to write the data into a file.]
count
0

To decide what number is the number of paradoxes caused:
	choose row 1 in the Table of Paradoxes;
	decide on the count entry.

To cause a paradox:
	say "[p][alert]*** Time unravels around you... ***[/alert][p]"; [This message is a reference to "All Things Devours" by Half Sick of Shadows, which was one of the big inspirations for this section.]
	choose row 1 in the Table of Paradoxes;
	increment the count entry;
	write the File of Paradoxes from the Table of Paradoxes;
	autorestore the game.

Chapter 2 - Causing

Paradox check is a rulebook. [This goes through the game state and searches for inconsistencies.]

Section A - Incorrect Player Location

To check location paradox for (expected - an object) and (actual - an object):
	if expected is actual, stop;
	say "[i]Your twin looks around at [the actual]. This isn't [the expected].[/i]";
	cause a paradox.

Paradox check (this is the location paradox rule):
	if the time-tick is zero, make no decision;
	choose row (time-tick) in the Table of Time-Travel Locations;
	if there is an alpha location entry, check location paradox for the alpha location entry and the holder of self alpha;
	if there is an beta location entry, check location paradox for the beta location entry and the holder of self beta;
	if there is an gamma location entry, check location paradox for the gamma location entry and the holder of self gamma;
	if there is an delta location entry, check location paradox for the delta location entry and the holder of self delta.

The location paradox rule is listed before the object visibility paradox rule in the paradox check rules.

Section B - Incorrect Object Location

Visibility marking relates various former selves to various things. The verb to mark implies the visibility marking relation.
Definition: something is marked for visibility rather than unmarked for visibility if something marks it.
The verb to be currently seen by means the reversed visibility relation.

Liminality relates a door (called X) to a room (called Y) when the front side of X is Y or the back side of X is Y. The verb to be liminal to implies the liminality relation.

Paradox check (this is the object visibility paradox rule):
	repeat with the subject running through on-stage former selves: [First, we go through and mark what each self can see, by means of a relation.]
		unless the subject is younger than the player, next; [Future selves won't cause paradoxes until the player gets to that time]
		repeat with the item running through things currently seen by the subject:
			if the item is the subject, next; [Don't bother recording this]
			if the item is scenery and the item is not a door, next; [Scenery is assumed to stay in the location]
			if the item is a backdrop, next; [Backdrop 'locations' are weird, so avoid them]
			now the item is marked by the subject; [But otherwise, mark that it's visible to the subject]
		let the place be the location of the subject;
		[unless the place is light-filled, next;]
		repeat with the item running through doors liminal to the place:
			now the item is marked by the subject;
	sort the Table of Sightings in timestamp column order; [We only want the visibility data from *this* turn, so we sort the table to ensure it's all together.]
	repeat through the Table of Sightings:
		if the timestamp entry is less than the time-tick, next; [This entry is from before. Don't need it.]
		if the viewer entry is the player, next; [This entry was just written. No need to check again.]
		if the timestamp entry is greater than the time-tick, break; [This entry is from afterward. We must be done with this turn, let's move on.]
		let found be false; [Did we find the same object, in the same state?]
		let partially found be false; [Did we find the same object, but in a different state (open/closed)?]
		if there is an item entry: [This is a single discrete item which cannot be duplicated, rather than a class like the scrolls or books.]
			if the viewer entry marks the item entry and (the holder of the item entry is the place entry or (the item entry is a door and the item entry is liminal to the place entry)): [Check where it is. If it's a normal thing, that'll be the location. But if it's a door, we need an additional check to see if the object is currently being simulated in the opposite room.]
				if (the item entry is a container or the item entry is a door) and the item entry is openable: [If this object has an obvious change of state, see if it's consistent with what we remember.]
					if the status entry is notably open and the item entry is closed:
						now partially found is true;
						next;
					otherwise if the status entry is notably closed and the item entry is open:
						now partially found is true;
						next;
				now found is true;
				now the item entry is not marked by the viewer entry; [This object has been found, remove it from consideration.]
		otherwise:
			let K be the type entry;
			repeat with the item running through things of kind K: [See if any item nearby fits the criterion.]
				if the viewer entry marks the item and the holder of the item is the place entry: [No need to check for doors here. Doors aren't duplicable.]
					if the item entry is a container and the item entry is openable:
						if the status entry is notably open and the item entry is closed:
							now partially found is true;
							next;
						otherwise if the status entry is notably closed and the item entry is open:
							now partially found is true;
							next;
					now found is true;
					now the item is not marked by the viewer entry;
					break;
		if found is false: [We didn't find a perfect match. Paradox!]
			let the item be nothing;
			if there is a type entry, let the item be a random thing of kind type entry; [We're matching against a kind, but we want to print the name of the object in the error message.]
			otherwise let the item be the item entry;
			if partially found is true: [We found the right object but in the wrong state.]
				say "[i]In [the location of the viewer entry], your twin sees [the item] standing [status entry].[/i]";
				cause a paradox;
			say "[i]In [the location of the viewer entry], your younger self looks for [if the item is a former self][regarding the player standin][their] twin[else][the item][end if][if the place entry is a thing or the place entry is not the location of the viewer entry] in [the place entry][end if], but [regarding the item][they] [are] not there.[/i]";
			cause a paradox;
	repeat with the item running through marked for visibility things: [Now we know everything we remember being here, is here. But there might be something extra as well.]
		let the viewer be a random former self who marks the item;
		if the item is a former self: [Special messages if two copies of the player see each other unexpectedly.]
			if the item is the player:
				say "[regarding the player standin]Your twin turns toward you, [their] eyes widening in surprise. You weren't there before...";
			otherwise:
				if the location of the item is the location:
					say "[regarding the player standin]Your twins turn, catching sight of each other for the first time. That didn't happen last time...";
				otherwise:
					say "[i]In [the location of the item], two of your twins suddenly catch sight of each other. That didn't happen before.[/i]";
		otherwise:
			say "[i]In [the location of the viewer], your younger self suddenly catches sight of [the item]. [Those] [were]n't there before.[/i]";
		cause a paradox;

To say were: [Because Inform doesn't by default implement past forms in a present-tense story]
	if the prior naming context is plural or the prior named object is the player, say "were";
	otherwise say "was".

Part 3 - Rules

Chapter 1 - Values

Time-travel management is a rulebook. [This manages all the time travel code necessary each turn.]

Mine event is a rulebook. [This deals with any events happening in the mine.]

The time-travel scheduling timer is a number that varies.
To decide which number is the/-- time-tick: decide on the time-travel scheduling timer.

Every turn when time-travelling (this is the time-travel management staging rule):
	follow the time-travel management rules.

Chapter 2 - Time-Travel Management

Last time-travel management rule (this is the record current self's action and location rule): [First]
	if the time-tick is zero, make no decision;
	choose row (time-tick) in the Table of Time-Travel Actions;
	let A be the most recent time-travel action;
	if the player is: [TODO: There has to be a more elegant way to do this.]
		-- self alpha:
			now the alpha entry is A;
		-- self beta:
			now the beta entry is A;
		-- self gamma:
			now the gamma entry is A;
		-- self delta:
			[now the delta entry is A;]
			do nothing; [No need to record--this will never be replayed.]
		-- otherwise:
			say "ERROR: tried to record action for [printed name of the player], which is not alpha, beta, gamma, or delta!";
	choose row (time-tick) in the Table of Time-Travel Locations;
	let the place be the holder of the player;
	if the player is:
		-- self alpha:
			now the alpha location entry is the place;
		-- self beta:
			now the beta location entry is the place;
		-- self gamma:
			now the gamma location entry is the place;
		-- self delta:
			now the delta location entry is the place.

Last time-travel management rule (this is the evaluate other selves' actions rule): [Second]
	if the time-tick is zero, make no decision;
	choose row (time-tick) in the Table of Time-Travel Actions;
	if self gamma is younger than the player:
		try the gamma entry;
	if self beta is younger than the player:
		try the beta entry;
	if self alpha is younger than the player:
		try the alpha entry.

The update object sightings rule is listed before the check for paradoxes rule in the time-travel management rules. [Third]

Last time-travel management rule (this is the check for paradoxes rule): [Fourth]
	follow the paradox check rules.

Last time-travel management rule (this is the time-travel mine flavor rule): [Fifth]
	follow the mine event rules.

Last time-travel management rule (this is the increment time-travel ticker rule):
	increment the time-travel scheduling timer.

Chapter Z - TEMP

Casting golmac is an action applying to nothing. Understand "golmac" as casting golmac.
Carry out casting golmac: say ">>>POOF<<<[p]"; cause time-travel.

Chapter 3 - Reporting

After someone going when the room gone from is adjacent (this is the describe footsteps leaving rule):
	if (the room gone to) is the location, continue the action;
	let the way be the best route from the location to (the room gone from), using even locked doors;
	say "You hear footsteps from [if the way is up]above[else if the way is down]below[else if the way is a direction][the way][else]nearby[end if]. It sounds as though someone is [if the noun is up]climbing upward[else if the noun is down]climbing downward[else if the noun is a direction]walking [noun]ward[else]leaving the room[end if].";
	continue the action.

After someone going when the room gone to is adjacent (this is the describe footsteps arriving rule):
	if (the room gone from) is the location, continue the action;
	let the way be the best route from the location to (the room gone to), using even locked doors;
	let the movement be the noun;
	if the movement is a direction, now the movement is the opposite of the movement;
	say "You hear footsteps from [if the way is up]above[else if the way is down]below[else if the way is a direction][the way][else]nearby[end if], as though someone is [if the movement is up]climbing upward[else if the movement is down]climbing downward[else if the movement is a direction]entering from [the movement][else]entering the room[end if].";
	continue the action.

Book II - Alpha

Chapter 1 - Initial Area

A mine area is a kind of room. A mine area is usually dark. The Mines are a region.

The Mine Entrance is a mine area. "This seems to be the entrance to a large coal mine. Everything is covered in coal dust, and the air feels thick and hard to breathe. Narrow passages snake off to the northeast and southwest, and a comfortably wide tunnel leads southeast."

Instead of going to the Mine Entrance when the steam is on-stage:
	say "A pipe of some sort seems to have broken, and the area ahead is filled with billowing steam. You try to press onward but the droplets begin to sting and burn your skin, and after a moment you are forced to turn back."

Some steam is a backdrop. Understand "billowing" or "scalding" or "cloud" or "droplets" as the steam.
Instead of touching the steam: say "Agh! It scalds your hand, and you quickly recoil from it."
Instead of smelling the steam: say "You aren't quite brave enough to stick your face in it, but the primary smell in the air is still coal dust."

The steamy area is a region in the Mines. The Mine Entrance is in the steamy area.

[
Effect of casting lesoch at the steam:
	say "The wind cuts through the steam, and for a brief moment a path is clear. You hurry through to the other side...";
	if the player is in the Equipment Room, now the player is in the Reactor Room;
	otherwise now the player is in the Equipment Room.
Inverse effect of casting lesoch at the steam:
	say "The steam seems to freeze and hang suspended in midair for a moment. But there is still pressure in the pipe, and the respite doesn't last long."
]

Section A - The Records Room and Paradox Hints

The Records Room is a mine area in the Mines, northeast of the Mine Entrance. "This room was evidently filled with neatly-organized rows of files and records, but half the ceiling has collapsed, filling the area with shattered bits of rock and debris. Scraps of paper cover the floor, most of them rendered illegible by tearing and water damage. Empty doorframes stand to the south and southwest."
A flickering sign is fixed in place in the Records Room. "An illuminated sign flickers feebly on one wall." The description of the sign is "[tt]~ [if the number of paradoxes caused is 0 or the Reactor Room is unvisited]-[one of]7[or]21[or]35[or]42[or]96[or][infinity][sticky random] weeks since the last temporal paradox[else]Paradoxes this week: [number of paradoxes caused][end if] ~[/tt]". Understand "illuminated" or "paradox" or "paradoxes" or "number" or "number of" or "count" as the flickering sign.
Some scraps of paper are scenery in the Records Room. "Most of the papers are illegible, but a few passages can be made out...[p][tt][one of]
	I would not want to bet against the possibility of time travel.[br]
	My opponent might have seen the future and know the answer.[br]
	(Stephen Hawking)[or]
	'But what if you went back and killed your own grandfather?'[br]
	He stared at me, baffled. 'Why the **** would I do that?'[br]
	(Stephen King, '11/22/63')[or]
	We shall embody [bracket]our[close bracket] viewpoint in a principle of self-consistency,[br]
	which states that the only solutions to the laws of physics that can occur[br]
	locally in the real Universe are those which are globally self-consistent.[br]
	(Friedman, Morris, Novikov, Echeverria, Klinkhammer, Thorne, Yurtsever;[br]
	'Cauchy Problem in Spacetimes with Closed Timelike Curves')[or]
	His older self had taught his younger self a language which[br]
	the older self knew because the younger self, after being taught,[br]
	grew up to be the older self and was, therefore, capable of teaching.[br]
	(Robert A. Heinlein, 'By His Bootstraps')[or]
	You couldn't have changed history.[br]
	But you could get it right to start with.[br]
	Do something differently the first time around.[br]
	(Eliezer Yudkowsky, 'Harry Potter and the Methods of Rationality')[then at random][/tt]".
Understand "papers" or "legible" or "illegible" or "scrap" as the scraps of paper. Instead of taking the scraps of paper: say "There are far too many to take all of them, and you doubt they'll be useful for anything." Instead of searching the scraps of paper, try examining the noun.
Rule for deciding whether all includes the scraps of paper: it does.

To say infinity:
	if Unicode 8734 is supported, say Unicode 8734; [∞]
	otherwise say "[bracket]infinity[close bracket]".

Section B - Set Decorating

The Equipment Room is a mine area in the Steamy Area, southwest of the Mine Entrance.

The Generator Room is a mine area in the Mines, southeast of the Equipment Room.

[TODO: puzzle here to activate the generator and power up the reactor]

A wooden barricade is a door, south of the Generator Room and north of the Difficult Passage. It is closed and not openable. "[if closed]The passage to the south has been blocked off by a hastily-constructed wooden barricade. Painted across it in red are the words [i]DANGER - CHRONIC INSTABILITY - CLOSED TO NON-SORCERERS UNTIL FURTHER NOTICE[/i][else]The metal post on one side of the [direction of the barricade from the location] door has collapsed, tearing the barricade to splinters[end if]."
Understand "hastily" or "constructed" or "wood" or "hastily-constructed" as the barricade. Understand "splinters" as the barricade when the barricade is open.
Instead of opening the closed barricade:
	say "Rushed they might have been, but whoever blocked off this door knew what they were doing. It would take far more strength than you possess to open it."
Instead of pushing or pulling or opening the open barricade:
	say "You push a few more pieces out of the way, widening the hole slightly."
[Effect of casting rezrov at the closed barricade:
	say "The spell doesn't quite take. This barrier isn't locked or sealed per say, just impassible, and the rezrov spell seems to be having difficulty dealing with it."
Empowered effect of casting rezrov at the closed barricade:
	say "The planks strain against their fastenings, and some of the nails tear themselves free of the wood. But it is difficult to focus the spell on something that isn't really a door or a container, and the magic fades before you make any significant progress."
Effect of casting rezrov at the open barricade:
	say "The pipe and some of the shattered planks twitch as though trying to move, but not much else happens."
]

The Difficult Passage is a mine area in the Mines. "This area is far tighter than you would like, and you have to walk sideways to keep your arms from pressing against the walls. You take a few deep breaths and try to remain calm. Thankfully the space ahead seems more open, and there is even a faint draft from the south."

Section C - The Reactor Itself

The Reactor Room is a mine area in the steamy area, southeast of the Mine Entrance, south of the Records Room, and northeast of the Generator Room. "This area is startling in its incongruity. The rock walls of the mine have been smoothed and flattened, forming a massive hemisphere."
A temporal reactor is fixed in place in the Reactor Room. "Machinery fills the room: enormous flywheels, colossal gears, chains thicker than your arm. [if the temporal reactor is inactive]Everything is perfectly still and quiet, and you have a sudden feeling of having stepped out of time, as though the miners might suddenly appear and continue their work[else]The reactor is whirling furiously[end if]." The printed name of the temporal reactor is "colossal machine". Understand "colossal" or "machine" or "machinery" as the temporal reactor. The temporal reactor can be inactive or active. The temporal reactor is inactive.

After going to the unvisited Reactor Room: [Reset the number of paradoxes if we restored from before this point.]
	choose row 1 in the Table of Paradoxes;
	now the count entry is 0;
	make no decision.

To activate the reactor:
	autosave the game;
	if we restored from an autosave: [The above line is the one we jump to when restoring, since that's when the save happened.]
		read the File of Paradoxes into the Table of Paradoxes;
		let x be the number of paradoxes caused;
		say "You hear a faint whirr as the temporal reactor slows to a stop. Everything seems to be just as it was at the moment you first turned it on[if x is 1].[p]What just happened? A paradox occurred[--]but shouldn't that be impossible? Something very strange is going on[else if x is 2].[p]Another paradox. But by definition that is impossible[else if x is 3].[p]If a paradox is impossible...then perhaps it didn't happen? And yet somehow you still remember it[else if x is 4].[p][i]It didn't happen.[/i] That makes more sense than your other explanations. And somehow the magic of the reactor allowed you to remember it[else if x is 5].[p]Given the papers in the Records Room, this makes some sense. When a paradox would have occurred (but didn't as that's impossible), the reactor allows the user to remember the hypothetical, and learn from it[end if].";
		try looking;
		now the temporal reactor is inactive;
	otherwise:
		say "A low humming noise begins, almost imperceptably at first, then gets louder and louder until it's almost unbearable. The flywheel of the reactor begins to spin, gaining momentum, until the spokes become a blur. The largest gear shudders and rotates slowly, pulling the chain with it. The air around the reactor begins to shimmer as if it were hot. There is a strange quality about the light: all the colors seem to have become brighter and more intense, especially right near the machinery.[p]But something is wrong. There is a [i]CLANG[/i] from the northwest, and steam begins to billow through the door. A moment later there is another [i]CLANG[/i] and a splintery tearing sound from the equipment room. A slightly-distorted voice echoes through the mine: [i]'Code Zeta. Repeat, Code Zeta. High temperatures detected in main generator. High risk of fire. Prepare to evacuate immediately.'[/i][br]";
		follow the epsilon transfer rule; [Will the player give their future self a spell book?]
		now the temporal reactor is active;
		now the barricade is open;
		move the steam to the steamy area;
		silently switch selves to self alpha;
		remove yourself from play;
		now the initialization of self alpha is the holder of the player;
		now the time-travel scheduling timer is zero.

Instead of switching on the temporal reactor: take full time; activate the reactor.

Chapter 2 - The Vault

Section A - The Waterfall Environs

The Waterfall Pool is a mine area in the Mines, south of the Difficult Passage. "The narrow passage opens here into a relatively spacious sort of dome, arcing high over your head. The floor has been flattened and smoothed out and you can faintly make out the tracks of some sort of machinery. But in the time since the mine was abandoned another tunnel has broken through the ceiling, and a steady cascade of dirty water splashes down, eroding a shallow pool into the floor. Various man-made entrances lead away in all directions, most rendered impassible by cave-ins or water."
The cascade is scenery in the Waterfall Pool. Understand "steady" or "waterfall" or "cascade of" or "water" or "fall" or "dirty" as the cascade. The description of the cascade is "It catches and scatters the light into mesmerizing patterns across the walls."
The shallow pool is scenery in the Waterfall Pool. Understand "water" or "pool of" or "dirty" as the shallow pool. The description of the shallow pool is "The flow of water has already made quite an impression in the stone floor."

Definition: something is waterfall-related if it is the cascade or it is the shallow pool or it is the stream of water.

Instead of eating or drinking or tasting a waterfall-related thing: say "The water tastes metallic and bitter."
Instead of inserting or throwing when the second noun is waterfall-related, say "That would just make a mess of [the noun]."
Instead of entering a waterfall-related thing, say "This doesn't seem the time for a swim."

The Sealed Passage is a mine area in the Mines. "This corridor is remarkably clean. The walls, floor, and ceiling are all made of some sort of metal, and all have the same width, giving the passage a perfectly square cross-section. It becomes slightly rougher to the west, with sections of the metal curling up from the underlying stone. A black door stands in an inconspicuous alcove to the southeast."

A red metal door is a closed door. It is west of the Waterfall Pool and east of the Sealed Passage.

The Unfinished Passage is a mine area in the Mines, west of the Sealed Passage. "The passage here is still perfectly square and perfectly straight, but the thin metal panels are bent upwards, revealing the stone underneath. The westmost parts have no panels at all, just bare stone. It doesn't look like they were pulled up: rather, it seems that this area was still being constructed when the mine was closed."

The metal panelling is a backdrop. It is in the Sealed Passage and the Unfinished Passage. The description of the panelling is "Flat pieces of thin metal, about one by one and a half meters in size. They were probably smoothed out by magic of some sort. They have been secured to the walls, floors, and ceilings of the passage, but toward the western end the panelling is unfinished and starting to pull away." Understand "flat" or "pieces" or "pieces of" or "paneling" or "panel" or "panels" or "thin" or "smooth" or "wall" or "walls" or "floor" or "ceiling" as the panelling.

The Dead End is a mine area in the Mines, west of the Unfinished Passage. "The tunnel ends in a flat wall of stone. A tracework of thin geometric-looking cracks spreads across it, emanating from a point at the exact center."

The tracework of geometric cracks is scenery in the Dead End. Understand "geometric-looking" and "looking" and "tracery" and "lines" and "pattern" and "square" and "spiral" and "flat" and "wall" and "wall of" and "stone" and "cracks in" as the tracework. The description of the tracework is "The pattern seems to be very regular. The lines are hair-thin, and all seem to run either horizontally or vertically. Examining it more closely, they form a sort of square spiral, starting at the center and spreading out toward the edges."

The Drafty Room is a mine area in the Mines, east of the Waterfall Pool. "Through some fortunate coincidence, the damage to the ceiling of this passage actually increased its size slightly. A strong draft is blowing in from the northeast."

The Crevasse is a mine area in the Mines, northeast of the Drafty Room. "The tunnel widens even further for a moment here, then narrows enough to be effectively impassible further to the northeast. A large part of the floor has collapsed downward into some lower tunnel, forming a deep sinkhole in the center of the room."

Instead of going northeast in the Crevasse: say "Correction: entirely impassible."
Instead of going down in the Crevasse: say "The hole seems treacherous, and you would likely fall if you tried to get too close."
Instead of jumping in the Crevasse: say "You glance again at the condition of the floor, and decide not to risk it."

The sinkhole is scenery in the Crevasse. Understand "crevasse" and "hole" and "sink hole" and "floor" and "collapsed" and "deep" as the sinkhole. The description is "It's hard to tell how deep it might be, and the edges don't look entirely safe to walk on. Climbing down safely would be difficult even for a more experienced caver."
Instead of entering the sinkhole: try going down.

Southeast of the Waterfall Pool is a mine area called Start of Tracks. The Start of Tracks is in the Mines. The description of Start of Tracks is "You are in a small side passage not far from the waterfall. A set of narrow-gauge metal rails begins here, running off toward the southeast. Unfortunately a cave-in has mostly destroyed the tracks and blocked off the tunnel they once used. A thin but rapid stream of water flows between the rails and loses itself somewhere among the boulders."

The rails are scenery in Start of Tracks. The description of the rails is "Likely used to transport coal up from the mines. Inanimate things like coal can actually be significantly harder to move with magic, especially in large enough quantities." Understand "tracks" or "narrow" or "gauge" or "narrow-gauge" or "metal" or "rail" or "track" as the rails.
Some boulders are scenery in Start of Tracks. The description of the boulders is "The ceiling seems to have come down, completely obliterating the tracks further down the tunnel." Understand "rocks" or "boulder" or "rock" or "cave in" or "cave-in" as the boulders.
The stream of water is scenery in Start of Tracks. The description of the stream is "It's not very deep, but would probably make your shoes unpleasantly wet." Understand "rapid" or "thin" or "trickle" or "flow" as the stream.
Instead of entering the stream:
	say "Your shoes are now unpleasantly wet. You step out again.";
	take full time.

Mine event when the time-tick is 5 and the player is younger than gamma (this is the drop scroll case rule):
	if the player is older than beta, make no decision;
	if the player can see the shallow pool:
		say "There is a loud [i]SPLASH[/i] as something falls from near the top of the waterfall and lands in the shallow pool.";
	if the location is adjacent to the Waterfall Pool:
		let the way be the best route from the location to the Waterfall Pool, using even locked doors;
		say "You hear a faint splashing sound to [the way].";
	let the item be a selection from waxy scrolls for iteration alpha;
	move the item to the Waterfall Pool.

Section B - The Vault Area

The black door is a door, southeast of the Sealed Passage and northwest of the Control Room. It is scenery. The description of the black door is "It slides into the wall along grooves in the floor and ceiling." Understand "narrow" or "groove" or "grooves" or "sliding" as the black door.

The Control Room is a mine area in the Mines. The description of the Control Room is "This room is incomplete, with [tt]under construction[/tt] signs affixed to the consoles at the far end, but it is far cleaner and shinier than the rest. One of the walls is covered by a wide glass screen showing the various levels of the mine. Another has various buttons and switches labelled in tiny writing. A narrow black door leads to the northwest, while a more open passage continues to the southeast."

The wide glass screen is scenery in the Control Room. The description of the screen is "Outlines of the mine glow red, behind the words [tt]FIRE HAZARD - ZETA - EVACUATE[/tt]." Understand "outlines" or "levels" or "outline" as the screen.

Southeast of the Control Room is the Outer Crypt. The description of the Outer Crypt is "This is the entrance to the mine proper: the 'Crypt', as the graffiti on the wall names it. In its unfinished state it looks more like a bank vault, with a complicated locking mechanism built into the wall, and half-connected wires and sensors trailing across the panels." The Outer Crypt is a mine area in the Mines.

Some graffiti is scenery in the Outer Crypt. The description of the graffiti is "Amid painted construction marks and designations are scrawled the words [i]Welcome to the Crypt - lasciate ogni speranza voi ch'entrate![/i]".
Some wires and sensors are scenery in the Outer Crypt. The description of the wires is "These were probably designed to detect fire and gas in the mines." Understand "half" or "connected" or "half-connected" as the wires.
The complicated lock is scenery in the Outer Crypt. Understand "locking" or "mechanism" as the lock. The description of the lock is "All of the gears and levers are connected to a wide dial in the center of the door."
The wide dial is part of the complicated lock. The description of the dial is "It can be turned to any number from 0000 to 9999."
The massive steel door is a locked door, south of the Outer Crypt and north of the Inner Crypt. "The massive steel door stands [if open]open[else]firmly closed[end if] to the [if the location is the Inner Crypt]nor[else]sou[end if]th."

The Inner Crypt is a mine area in the Mines. "After the impressive door and lock you expected more. But this is where construction must have ended: a wide, very square room hollowed out of the rock, extending far upward into darkness. Scratches in the floor indicate that the shaft would have been extended downwards as well to connect to the existing mines below."

Book III - Beta

Book IV - Gamma

Book V - Delta

Book VI - Epsilon?

The player carries your spell book. To cough is a verb.

The spellbook given flag is initially false.
The dirty figure is a fixed in place person. "Your twin lies here, gasping for breath." Understand "self" or "twin" or "man" or "woman" or "future" or "older" as the dirty figure. The description of the dirty figure is "In all physical respects [regarding the player standin][they] could be your twin."

This is the epsilon transfer rule:
	say "Before your eyes a section of the wall comes down in a shower of wood and stone. A figure staggers through. [regarding the player standin][Their] clothes are dirty and torn, but [they] [look] unsettlingly like you.
	
	'Give[--]me[--]' [they] [cough], gasping for air. '...spellbook...understand[--]'";
	move the dirty figure to the location;
	the figure departs in one turn from now.

At the time when the figure departs:
	say "Your twin collapses, whispering something unintelligible, and disappears into thin air.";
	remove the dirty figure from play.

Instead of giving your spell book to the dirty figure:
	say "The figure seizes the spell book, falling to [regarding the player standin][their] knees, and coughs again.";
	remove your spell book from play;
	now the spellbook given flag is true;
	take full time.

[Effect of casting golmac at when the player carries the cube:
	say "You croak out the now-familiar spell one more time, barely managing to spit out the incantation through the thick smoke. Light, sound, the world distorts...and the wall is collapsing again, but you're on the other side now. You remember this from before.
	
	As though in a dream, you stagger forward through the gap, see yourself looking up in confusion as the reactor activates, looking straight at you. But there is no time to explain. 'Give[--]me[--]spellbook[--]' you croak between coughs. '...understand...'[r][p]";
	now the player is yourself;
	if the spellbook given flag is true:
		say "And sure enough, your twin hands over [regarding the player standin][their] spell book, watching you fearfully. You should thank them...but no time.";
		move your spell book to the player;
	otherwise:
		say "But no! [regarding the player standin][They] [do]n't understand! [They] [look] at you, [look] at [their] book, do nothing...and there is no more time.";
	if blorple is listed in the player's memory:
		say "You look at the cube and whisper [i]blorple[/i]. The prepared spell goes off, leaving your memory in a rush, and just as everything goes black, the world reshapes itself...";
	otherwise if the player carries your spell book:
		say "You fumble through the pages, searching for the spell you need. Just a second more! There it is...you go through the incantations as quickly as you can, gesturing and whispering, and then just as you can feel your grip on consciousness fading, the world changes...";
	otherwise:
		say "No magic...no book...no time...you stagger forward, reaching for the reactor, but the lack of air is taking its toll. Your legs give out, your vision going fuzzy...
		
		The other 'you' watches as [regarding the player standin][their] twin collapses. But the body doesn't disappear; it remains on the floor in front of [them].";
		cause a paradox;
	move the player to the Perfect Cube;
	award points for "manipulating time";
	end the story saying "To be continued..."]

The Perfect Cube is a room.