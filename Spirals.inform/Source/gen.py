#!/usr/bin/env python3

from sys import argv

print('[' + argv[0] + ' ' + argv[1] + ' ' + ' '.join('"{}"'.format(arg) for arg in argv[2:]) + ']' + '\n')

for arg in argv[2:]:
	print('[{}]'.format(arg))
	print('There are {1} {0}s in Temporal Limbo. A {0} has a temporal iteration. A {0} has an object called the initialization.\n'.format(arg, argv[1]))
